import threading
from queue import Queue

from domain import *
from files_actions import *
from spider import Spider
import argparse

# Initiate the parser
site_name_argument_name = 'name'
site_url_argument_name = 'url'
number_of_threads_argument_name = 'thread'
parser = argparse.ArgumentParser()
parser.add_argument("-N", "--{}".format(site_name_argument_name), help="site name to be crawled")
parser.add_argument("-U", "--{}".format(site_url_argument_name), help="url to be crawled")
parser.add_argument("-T", "--{}".format(number_of_threads_argument_name), type=int, help="number of threads")

# Reads arguments and set them, if not exist asking from the user.
# Read arguments from the command line
args = parser.parse_args()

PROJECT_NAME = ''
BASE_URL = ''
NUMBER_OF_THREADS = 8  # best is 8
if args.__getattribute__(site_name_argument_name):
    PROJECT_NAME = args.__getattribute__(site_name_argument_name)
else:
    PROJECT_NAME = input("enter the site name")

if args.__getattribute__(site_url_argument_name):
    BASE_URL = args.__getattribute__(site_url_argument_name)
else:
    BASE_URL = input("enter the url to be crawled")

if args.__getattribute__(number_of_threads_argument_name):
    NUMBER_OF_THREADS = args.__getattribute__(number_of_threads_argument_name)

DOMAIN_NAME = get_domain_name(BASE_URL)
QUEUE_FILE = PROJECT_NAME + '/queue.txt'
CRAWLED_FILE = PROJECT_NAME + '/crawled.txt'
queue = Queue()
Spider(PROJECT_NAME, BASE_URL, DOMAIN_NAME)


def create_jobs_if_needed():
    queued_links = file_to_set(QUEUE_FILE)
    if len(queued_links) > 0:
        print(str(len(queued_links)) + ' Links in the queue ')
        create_jobs()


# this job is very easy and not taking time for the machine, only put to tasks (urls) to the queue
def create_jobs():
    for link in file_to_set(QUEUE_FILE):
        queue.put(link)

        # This makes the function not continue till enough queue.task_done() is called.
        queue.join()
        create_jobs_if_needed()


# creating worked as the numbers of threads in NUMBER_OF_THREADS var
def create_workers():
    for _ in range(int(NUMBER_OF_THREADS)):
        t = threading.Thread(target=work)
        t.daemon = True
        t.start()


def work():
    while True:
        url = queue.get()
        Spider.crawl_page(threading.current_thread().name, url)
        queue.task_done()


# This call create the workers that are crawl the urls in the queue
create_workers()

# this call is for the Main thread to create jobs, it will create job(urls) as the workers take care of the urls that
# are already inside the queue.
create_jobs_if_needed()
